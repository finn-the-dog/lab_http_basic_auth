# Basic http authentication

[Описание](https://developer.mozilla.org/ru/docs/Web/HTTP/%D0%90%D0%B2%D1%82%D0%BE%D1%80%D0%B8%D0%B7%D0%B0%D1%86%D0%B8%D1%8F)

### Требования:
- `flask`
- `flask_httpauth`

## Запуск
1. `make run` - запуск веб сервера
2. `make curl` - curl запрос с basic authentication к защищенному URL
3. `make command` - аналогичный запрос в виде скрипта на python

## Описание модулей
1. `bin/server/web.py` - веб-сервер, запускается на 0.0.0.0:5000. 0.0.0.0:5000/protected - защищенный URL (требует аутентификации)
2. `bin/server/users.py` - файл с пользователями:
 - admin - имеет доступ к защищенному URL
 - user - не имеет доступа
3. `bin/client/http_command.py` - скрипт с запросом к защищенному URL, в теле которого передаются логин и пароль для пользователя "admin"
