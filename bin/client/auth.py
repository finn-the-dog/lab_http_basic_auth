from string import Template
from .get_login_password import get_login_password

def auth(request):
    def wrapper(*args, **kwargs):
        resp = request(*args, **kwargs)
        unauthorized = lambda r: r == 'Unauthorized Access'
        if not unauthorized(resp):
            return resp

        login, password = get_login_password()
        auth = '-u {0}:{1}'.format(login, password)
        template = Template(kwargs['cmd'])
        auth_cmd = template.safe_substitute(auth=auth)
        print(auth_cmd)
        resp = request(auth_cmd)
        return resp

    return wrapper
