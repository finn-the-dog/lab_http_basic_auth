from socket import gethostname as socket_gethostname
import string
import argparse
from string import Template
from .execute import execute
def main():

    parser = argparse.ArgumentParser(description='Protected command', prog='dnadd_center')

    args = parser.parse_args()

    full_hostname = socket_gethostname()
    hostname = full_hostname.split('.')[0]
    port = 5000
    data = '\'\''
    command = 'protected'
    method = 'POST'

    template = 'curl $auth -d {data} -X {method} http://{host}:{port}/{command}'

    cmd = template.format(data=data, method=method, host=hostname, port=port, command=command)

    resp = execute(cmd=cmd)
    
    print(resp)

if __name__ == '__main__':
    main()
