from subprocess import Popen, PIPE
from string import Template
from .auth import auth

@auth
def execute(cmd):
    template = Template(cmd)
    ex_cmd = template.safe_substitute(auth='')
    process = Popen(ex_cmd, shell=True, stdout=PIPE, stderr=PIPE)
    output, error = process.communicate()

    resp = output.decode()

    return resp
