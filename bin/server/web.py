import os
from flask import Flask, request, jsonify, redirect, session, g
from flask_httpauth import HTTPBasicAuth
from .users import USERS

app = Flask(__name__)
app.config['SECRET_KEY'] = os.urandom(24)

auth = HTTPBasicAuth()

@auth.verify_password
def verify_password(username, password):
    user = USERS.get(username)
    if user:
        print(user.get('password'), password, user)
    if user and user.get('admin') and user.get('password') == password:
        g.user = user
        return True
    return False

@app.route('/protected', methods=['GET', 'POST'])
@auth.login_required
def protected():
    return 'logged\n'# in as: {}'.format(current_user.id)
